# README #

Download the entire repo and open the project in LabVIEW. Run the demo2.vi to see the demo in action

### What is this repository for? ###

* An API for LabVIEW to control Pane resizing and a context based UI.
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
For more details go to [https://decibel.ni.com/content/docs/DOC-38428](https://decibel.ni.com/content/docs/DOC-38428)